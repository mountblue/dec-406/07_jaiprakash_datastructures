/**addNode() function adds node at last  */
function addNode(root, data) {
  var node = {
    value: data,
    next: null
  };
  if (root === null) {
    return node;
  }
  else if (root.next === null) {
    root.next = node;
  }
  else {
    var temp = root;
    while (temp.next != null) {
      temp = temp.next;
    }
    temp.next = node;
  }

}

/**printListEvenOdd() function prints the node first even than odd  */
function printListEvenOdd(root) {
  var temp = root;
  var even = [];
  var odd = [];
  while (temp != null) {
    if (temp.value % 2 === 0)
      even.push(temp.value);
    else
      odd.push(temp.value);
    temp = temp.next;
  }
  even = even.concat(odd);
  return even;
}

/**printList() function just traverse the list */
function printList(root) {
  var temp = root;
  while (temp != null) {
    console.log(temp.value);
    temp = temp.next;
  }
}

/**run() function will be invoked when user click the run button on browser.
 * & this function invokes the printListEvenOdd() method
 */
function run() {
  let root = addNode(null, 17);
  addNode(root, 15);
  addNode(root, 8);
  addNode(root, 9);
  addNode(root, 2);
  addNode(root, 4);
  addNode(root, 6);
  console.log("List Items");
  printList(root);
  var output = printListEvenOdd(root);
  console.log("List Items Even Followed by Odd");
  console.log(output);

}




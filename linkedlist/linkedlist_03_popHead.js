let headNode = null;
/**addNode() function adds node at last  */
function addNode(head, data) {
  var node = {
    value: data,
    next: null
  };
  if (head === null) {
    return node;
  }
  else if (head.next === null) {
    head.next = node;
  }
  else {
    var temp = head;
    while (temp.next != null) {
      temp = temp.next;
    }
    temp.next = node;
  }

}

/**printList() function just traverse the list */
function printList(head) {
  var temp = head;
  while (temp != null) {
    console.log(temp.value);
    temp = temp.next;
  }
}

/**pop() function will remove the head node &return the new head */
function pop(head) {
  headNode = head.next;
  return head.value;
}

/**run() function will be invoked when user click the run button on browser.
 * & initialize the list & invokes the pop() method
 */
function run() {
  headNode = addNode(null, 17);
  addNode(headNode, 15);
  addNode(headNode, 8);
  addNode(headNode, 9);
  addNode(headNode, 2);
  addNode(headNode, 4);
  addNode(headNode, 6);
  //console.log(head.value);;
  printList(headNode);
  item = pop(headNode);
  console.log("Poped Item =" + item);
  console.log("List Items After Pop Operation");
  printList(headNode);


}




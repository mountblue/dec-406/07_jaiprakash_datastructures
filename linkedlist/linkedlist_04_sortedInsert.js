let headNode = null;
/**push() function adds node at last  */
function push(head, data) {
  var node = {
    value: data,
    next: null
  };
  if (head === null) {

  }
  else if (head.next === null) {
    head.next = node;

  }
  else {
    var temp = head;
    while (temp.next != null) {
      temp = temp.next;
    }
    temp.next = node;
  }
  return node;
}
/**sortedInsert() function inserts the data into list at its correct position
 * sortedInsert() function takes head of the list as input &
 * return the new head if changed;
  */
function sortedInsert(head, data) {
  var node = {
    value: data,
    next: null
  };
  if (head === null) {
    return node;
  }
  else if (head.next === null) {
    if (head.value > data) {
      node.next = head;
      return node;
    }
    else
      head.next = node;
    return head;
  }
  else {
    var temp = head;
    var prev = head;
    while (temp != null) {
      if (temp.value >= data)
        break;
      else {
        prev = temp;
        temp = temp.next;
      }
    }
    node.next = prev.next;
    prev.next = node;

  }
  return head;
}
function printList(head) {
  var temp = head;
  while (temp != null) {
    console.log(temp.value);
    temp = temp.next;
  }
}

/**run() function will initialize the list and invoked by browser run buttton
 * &calls sortedInsert() method
  */
function run() {
  headNode = push(null, 2);
  push(headNode, 4);
  push(headNode, 6);
  push(headNode, 8);
  push(headNode, 9);
  push(headNode, 15);
  push(headNode, 17);
  printList(headNode);
  sortedInsert(headNode, 7);
  console.log("List Items After Insert Operation");
  printList(headNode);


}




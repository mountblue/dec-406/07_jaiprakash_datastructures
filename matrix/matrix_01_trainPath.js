/**run() function initialize the matrix and path for the train and print yes if Path is possible
 * otherwise it prints NO.
 */
function run() {
	var n = 2;
	var m = 3;
	var path = ['l', 'l', 'r', 'u'];
	var flag = 0;
	let pathCount = 0;
	for (let i = 0; i < n; i++) {
		for (let j = 0; j < m; j++) {
			flag = isPathPossible(i, j, n, m, path, pathCount);
			if (flag === 1)
				break;
		}
		if (flag === 1)
			break;
	}
	console.log("Path Possible : " + ((flag === 1) ? "YES" : "No"));
}

/**isPathPossible () takes left ,right ,up down position and path array as input &
 * returns bollean values if path ispossible it returns true otherwise false
 */
function isPathPossible(i, j, n, m, path, tempPathCount) {

	if (i < 0 || i >= n || j < 0 || j >= m)
		return 0;
	if (tempPathCount > path.length)
		return 0;
	if (path[tempPathCount] === 'l') {
		j -= 1;
		return isPathPossible(i, j, n, m, path, ++tempPathCount);
	}
	if (path[tempPathCount] === 'r') {
		j += 1;
		return isPathPossible(i, j, n, m, path, ++tempPathCount);
	}
	if (path[tempPathCount] === 'u') {
		i -= 1;
		return isPathPossible(i, j, n, m, path, ++tempPathCount);
	}
	if (path[tempPathCount] === 'd') {
		i += 1;
		return isPathPossible(i, j, n, m, path, ++tempPathCount);
	}
	return 1;
}


var soduku;
/**run() function initialize the soduku  print yes if solution is possible
 * otherwise it prints NO.
 */
function run() {
  soduku = [[3, 0, 6, 5, 0, 8, 4, 0, 0],
  [5, 2, 0, 0, 0, 0, 0, 0, 0],
  [0, 8, 7, 0, 0, 0, 0, 3, 1],
  [0, 0, 3, 0, 1, 0, 0, 8, 0],
  [9, 0, 0, 8, 6, 3, 0, 0, 5],
  [0, 5, 0, 0, 9, 0, 6, 0, 0],
  [1, 3, 0, 0, 0, 0, 2, 5, 0],
  [0, 0, 0, 0, 0, 0, 0, 7, 4],
  [0, 0, 5, 2, 0, 6, 3, 0, 0]];
  var isSolution = true;
  for (let i = 0; i < soduku.length; i++) {
    for (let j = 0; j < soduku[i].length; j++) {
      if (isNotUnique(i, j, soduku[i][j])) {
        console.log("i = " + i + "...j= " + j);
        isSolution = false;
        break;
      }
      if (!isSolution)
        break;
    }
  }
  console.log(isSolution);

}

/**isNotUnique() takes row ,coloumn ,and soduku as input &
 * returns boolean value
 */
function isNotUnique(i, j, item) {

  //  console.log(soduku)
  for (let k = 0; k < soduku[i].length; k++)
    if (k != j && soduku[i][k] === item && item !== 0)
      return true;

  for (let k = 0; k < soduku.length; k++)
    if (k != i && soduku[k][j] === item && item !== 0)
      return true;
  return false;
}

/** Node class represent the datatype for  the tree node which contains value feild for data &
 * left feild for left node &right field for right node
 */
class Node {
  constructor(data) {
    this.left = null;
    this.value = data;
    this.right = null;
  }
}

/**getBinaryTree() returns the binary search tree */
function getBinaryTree() {
  root = new Node(1);
  root.left = new Node(2);
  root.right = new Node(3);
  root.left.left = new Node(4);
  root.left.right = new Node(5);
  root.right.left = new Node(6);
  root.right.right = new Node(7);
  root.left.left.left = new Node(8);
  root.right.left.left = new Node(9);
  root.right.left.right = new Node(10);
  return root;
}

/** isValuePresent()takes node of the tree and data to be checked as input and returns boolean value
 * if the data is present it returns true otherwise it returns false
 */
function isValuePresent(node, data) {
  if (node == null)
    return false;
  if (node.value == data)
    return true;
  let flag = false;
  flag = isValuePresent(node.left, data);
  if (flag)
    return flag;
  flag = isValuePresent(node.right, data);
  if (flag)
    return flag;
  return flag;
}

/**turnsFromRoot() takes root of the tree ,data and previous turn as input And
 * returns the totalnumber of turns required to reach from one node to another
 * here we use previousTurn which store the privious move.if the our move for value is in other than
 * previousTurn than only we add 1 in turns otherwise we didnt take turn so nothing to add. 
 */
function turnsFromRoot(root, data, previousTurn) {
  if (root == null)
    return 0;
  if (root.value == data)
    return 0;
  if (isValuePresent(root.left, data))
    return (previousTurn == "left" ? turnsFromRoot(root.left, data, "left") : turnsFromRoot(root.left, data, "left") + 1);
  if (isValuePresent(root.right, data))
    return (previousTurn == "right" ? turnsFromRoot(root.right, data, "right") : turnsFromRoot(root.right, data, "right") + 1);
}

/**totalTurns() takes root of the tree ,2 nodes values & 
 * returns the total turns required from value 1 to value2.
 * here the concept is first check for the root value if any of the value1 or value2 is equal to the 
 * root value.than we have to just returns the total turns required from the root.
 * if the root value is not equal to the any of both values(valu1,value2) than 
 * we have to check  whether both this value is present in which sub tree.
 * if both values are in same sub tree just we again call totalTurns(for this sub tree).
 * if both values are not in same tree than just calculate total number of turns required from root 
 * to the given value & do this for both values.and add both these turns to get total turns 
 *   */
function totalTurns(root, data1, data2) {
  let totalTurns = 0;
  if (root == null)
    return 0;
  if (data1 == root.value && isValuePresent(root.left, data2)) {
    return turnsFromRoot(root.left, data2, "left") + 1;
  }
  else if (data2 == root.value && isValuePresent(root.left, data1)) {
    return turnsFromRoot(root.left, data1, "left") + 1;
  }

  if (data1 == root.value && isValuePresent(root.right, data2)) {
    return turnsFromRoot(root.right, data2, "root") + 1;
  }
  else if (data2 == root.value && isValuePresent(root.left, data1)) {
    return turnsFromRoot(root.right, data1, "root") + 1;
  }
  if (isValuePresent(root.left, data1) && isValuePresent(root.left, data2))
    return totalTurns(root.left, data1, data2);
  if (isValuePresent(root.right, data1) && isValuePresent(root.right, data2))
    return totalTurns(root.right, data1, data2);
  if (!(isValuePresent(root.left, data1) && isValuePresent(root.left, data2))) {
    if ((isValuePresent(root.left, data1)) && (isValuePresent(root.right, data2))) {
      totalTurns += turnsFromRoot(root.left, data1, "left") + 1;
      totalTurns += turnsFromRoot(root.right, data2, "right") + 1;
    }
    else if ((isValuePresent(root.right, data1)) && (isValuePresent(root.left, data2))) {
      totalTurns += turnsFromRoot(root.right, data1, "right");
      totalTurns += turnsFromRoot(root.left, data2, "left");
    }
  }
  return totalTurns;
}

/**run() initialize the binary tree and call maxDiff() function */
function run() {
  let root = getBinaryTree();
  // console.log(root.value)
  let data1 = 5;
  let data2 = 10;
  let turns = totalTurns(root, data1, data2);
  console.log(turns);
}
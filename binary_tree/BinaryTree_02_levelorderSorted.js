/** Node class represent the datatype for  the tree node which contains value feild for data &
 * left feild for left node &right field for right node
 */
class Node {
  constructor(data) {
    this.left = null;
    this.value = data;
    this.right = null;
  }
}

/**getBinaryTree() returns the binary search tree */
function getBinaryTree() {
  root = new Node(1);
  root.left = new Node(3);
  root.right = new Node(2);
  root.left.left = new Node(7);
  root.left.right = new Node(6);
  root.right.left = new Node(5);
  root.right.right = new Node(4);
  return root;

}
/**sortNumber() return difference of two integers &
 * used by javascript sort method to sort array elements
 */
function sortNumber(a, b) {
  return a - b;
}
/**levelTraverse() takes the list of nodes(which are at same level) 
 * as input and print all nodes at same level in sorted oreder
 */
function levelTraverse(nodeList) {
  if (nodeList.length === 0)
    return;
  let elements = [];
  let levelNodes = [];
  while (nodeList.length != 0) {
    let node = nodeList.shift();
    elements.push(node.value);
    if (node.left != null && node.right != null) {
      levelNodes.push(node.left);
      levelNodes.push(node.right);
    }
  }
  elements.sort(sortNumber);
  console.log(elements);
  levelTraverse(levelNodes);

}
/** levelOrderTraversal() takes root of the tree and traverse the tree in levelorder */
function levelOrderTraversal(root) {
  if (root == null) {
    console.log("tree is empty");
    return;
  }
  let items = [];
  items.push(root);
  console.log(items.shift().value);
  items.push(root.left);
  items.push(root.right);
  levelTraverse(items);
}

/**run() initialize the binary tree and call maxDiff() function */
function run() {
  let root = getBinaryTree();
  // console.log(root.value)
  levelOrderTraversal(root);
}
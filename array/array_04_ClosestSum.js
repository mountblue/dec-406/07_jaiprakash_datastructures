/* run() is the function which will invoked when user click the run button on the browser
and run() initalize the array and print the Closest sum of array to the given sum */
function run() {
    var array1 = [1, 4, 5, 7];
    var array2 = [10, 20, 30, 40];
    var sum = 32;
    console.log("output For 1st Test Cases");
    closestSum(array1, array2, sum);
    sum = 50
    console.log("output For 2nd Test Cases");
    closestSum(array1, array2, sum);
}
/*ClosestSum() is  function which takes 2 arrays and sum to be checked and results the pair
 *pair is a variable which stores the pair 
 *in this function 1st element of array1 will check with all elements of array2 and then second element
 *of array  array1 will check with all elements of array2 and continues till we find the desired result */
function closestSum(arr1, arr2, sum) {
    var pair = {};
    var temp = sum - (arr1[0] + arr2[0]);
    console.log("temp = " + temp);
    for (let i = 0; i < arr1.length; i++) {
        for (let j = 0; j < arr2.length; j++) {
            //console.log("temp = "+temp+" sum= "+(arr1[i]+arr2[j]));
            if (temp > (sum - (arr1[i] + arr2[j])) && (sum - (arr1[i] + arr2[j])) >= 0) {
                pair.x = arr1[i];
                pair.y = arr2[j];
                temp = sum - (arr1[i] + arr2[j]);
            }

        }
    }
    console.log("pair");
    console.log(pair);

}
/* run() is the function which will invoked when user click the run button on the browser
and run() initalize the array and print the missing number In Sequence */
function run() {
  var array1 = [1, 3, 4, 5, 7];
  var array2 = [1, 2, 3, 4, 5, 6];
  var k1 = 2;
  var k2 = 2;
  var missingNumber = missingInSequence(array1, k1)
  console.log("Missing Number in 1st Sequence");
  console.log(missingNumber);
  missingNumber = missingInSequence(array2, k2)
  console.log("Missing Number in 2nd Sequence");
  console.log(missingNumber);
}
/* missingInSequence() function returns the missing number in an array*/
function missingInSequence(array, k) {
  var count = 0;
  var first = array[0];
  for (i = 0; i < array.length; i++ , first++) {

    if (first !== array[i]) {
      count++;
      first = array[i];
      if (count == k)
        break;

    }
  }
  return count == 0 ? -1 : --first;
}
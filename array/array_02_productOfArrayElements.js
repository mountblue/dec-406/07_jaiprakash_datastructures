/** run() is the function which will invoked when user click the run button on the browser
and run() initalize the array and print the the product of array elements */
function run() {
    let array1 = [3, 6, 4, 8, 9];
    let product = productOfArrayElements(array1);
    console.log(product);
}
/** productOfArrayElements() returns the product of all elements*/
function productOfArrayElements(array) {
    let result = 1;
    for (let i = 0; i < array.length; i++)
        result *= array[i];
    return result;
}
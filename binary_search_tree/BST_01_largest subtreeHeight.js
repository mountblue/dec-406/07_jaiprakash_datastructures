/** Node class represent the datatype for  the tree node which contains value feild for data &
 * left feild for left node &right field for right node
 */
class Node {
  constructor(data) {
    this.left = null;
    this.value = data;
    this.right = null;
  }
}

/**getBinaryTree() returns the binary search tree */
function getBinaryTree() {
  root = new Node(50);//addNode(50);
  root.left = new Node(30);// addNode(30);
  root.right = new Node(60);//addNode(60);
  root.left.left = new Node(5);//addNode(5);
  root.left.right = new Node(20);//addNode(20);
  root.right.left - new Node(50);//addNode(45);
  root.right.right = new Node(70);// addNode(70);
  root.right.right.left = new Node(65);//addNode(65);
  root.right.right.right = new Node(80);// addNode(80);
  return root;
}

/**largestBST() function will return the lrgest height of subtree which is also a binary search tree 
 * here the concept is check the root node whether it is BST or not if it is BST than return the height
 * else than apply the same thing i.e check for BST for its left child and right child 
*/
function largestBst(root) {
  if (root == null)
    return 0;
  if (isBST(root))
    return getHeight(root);
  else {
    let leftSubTreeHeight = 0;
    let rightSubTreeHeight = 0;
    let isLeftSubTreeBst = isBST(root.left);
    let isRightSubTreeBst = isBST(root.right);
    if (isLeftSubTreeBst)
      leftSubTreeHeight = getHeight(root.left);
    else {
      leftSubTreeHeight = largestBst(root.left);
    }
    if (isRightSubTreeBst)
      rightSubTreeHeight = getHeight(root.right);
    else {
      rightSubTreeHeight = largestBst(root.right);
    }

    return leftSubTreeHeight > rightSubTreeHeight ? leftSubTreeHeight : rightSubTreeHeight;
  }
}

/**getHeight() function will takes root of the tree as input & returns the height of tree */
function getHeight(root) {
  if (root == null)
    return 0;

  var rheight = 0;
  var lheight = 0;
  if (root.left !== null)
    lheight = getHeight(root.left);
  if (root.right !== null)
    rheight = getHeight(root.right);
  return (lheight > rheight ? lheight + 1 : rheight + 1);
}

/**IsBST() is a function which takes root of the tree as input and reuturns the boolean value
 * here the concept is check root's value is greater than its left child & root's value less than 
 * its right child if both condition are than check this condition recursively  for its left &
 * right child wherever this condition fails it will return flase.Otherwise it returns true
 */
function isBST(root) {
  let flag = true;
  if (root == null)
    return flag;
  //if ((root.left != null && root.left.value <= root.value) && (root.right != null && root.right.value >= root.value))
  if (root.left != null && root.left.value <= root.value) {
    flag = isBST(root.left);
    if (!flag)
      return flag;
  }
  else if (root.left != null && root.left.value > root.value)
    return false;
  if (root.right != null && root.right.value >= root.value) {
    flag = isBST(root.right);
    if (!flag)
      return flag;
  }
  else if (root.right != null && root.right.value < root.value)
    return false;

  return flag;

}

/**run() function will initialize the binary search tree
 * and invokes the largestBst() function
 */
function run() {
  var root = getBinaryTree();
  console.log(root);
  console.log(typeof root);
  console.log(isBST(root));
  var height = largestBst(root);
  console.log(height);
  //console.log(isBST(root));
}
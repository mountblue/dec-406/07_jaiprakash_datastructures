/**addNode() fuhnction will add the node to the tree which takes value of the node as input */
function addNode(data) {
  var node = {
    value: data,
    left: null,
    rigth: null
  };
  return node;
}

/** getBinaryTree() returns the binary search tree */
function getBinaryTree() {
  root = addNode(4);
  root.left = addNode(2);
  root.right = addNode(5);
  root.left.left = addNode(1);
  root.left.right = addNode(3);
  root.right.right = addNode(6);
  root.right.right.right = addNode(7);
  root.right.right.right.right = addNode(8);
  return root;
}

/**IsBST() is a function which takes root of the tree as input and reuturns the boolean value
 * here the concept is check root's value is greater than its left child & root's value less than
 * its right child if both condition are than check this condition recursively  for its left &
 * right child wherever this condition fails it will return flase.Otherwise it returns true
 */
function isBST(root) {
  let flag = true;
  if (root == null)
    return flag;
  //if ((root.left != null && root.left.value <= root.value) && (root.right != null && root.right.value >= root.value))
  if (root.left != null && root.left.value <= root.value) {
    flag = isBST(root.left);
    if (!flag)
      return flag;
  }
  else if (root.left != null && root.left.value > root.value)
    return false;
  if (root.right != null && root.right.value >= root.value) {
    flag = isBST(root.right);
    if (!flag)
      return flag;
  }
  else if (root.right != null && root.right.value < root.value)
    return false;

  return flag;

}
/**run() function will initialize the binary search tree &data to be deleted
 * and invokes the isBST() function
 */
function run() {
  var root = getBinaryTree();
  console.log(isBST(root));
}
/**run() function initialize the graph and graph is implemented using matrix */
function run() {
  let graph = [[0, 1, 0, 0, 0, 0, 0],
  [0, 0, 1, 1, 1, 0, 0],
  [0, 0, 0, 0, 1, 0, 0],
  [0, 0, 0, 0, 1, 0, 0],
  [0, 0, 0, 0, 0, 1, 0],
  [0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 1, 0, 0, 0]];

  evenDistancePair(graph);

}

/**evenDistancePair() function takes graph as input 
 * here the concept is starting the one vertex we are checking for all remaining vertex
 * whether this remainig vertex are at even length or not if it is at even length than 
 * we are adding the vertex even distance list;
 * & to traverse we just using dfs with some modification
 */
function evenDistancePair(graph) {
  let visited = [];

  for (let u = 0; u < graph.length; u++) {
    for (let k = 0; k < graph.length; k++)
      visited[k] = 0;

    visited[u] = 1;
    var pathCount = 1;
    var pathList = [];
    dfs(graph, visited, u, pathCount, pathList);
    for (let k = 0; k < pathList.length; k++) {
      console.log(u + "..." + pathList[k]);
    }
  }
}

/**dfs() is the function for depth first travesal
 * the concept is make an visited array of size total no vertex and initalize it with 0;
 * whenever we visit a vertex for first time make its value as 1 so at another iteration
 * we dont have to traverse this vertex;
 */
function dfs(graph, visited, u, pathCount, pathList) {
  for (let v = 0; v < graph.length; v++) {
    if (graph[u][v] == 1 && visited[v] != 1) {
      // console.log(v);
      //console.log(pathCount % 2 == 0)
      if (pathCount % 2 == 0)
        pathList.push(v);
      visited[v] = 1;
      dfs(graph, visited, v, pathCount + 1, pathList);
    }
  }
}

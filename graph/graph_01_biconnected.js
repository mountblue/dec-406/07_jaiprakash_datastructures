/**run() function initialize the graph and graph is implemented using matrix */
function run() {
  let graph1 = [[0, 1, 0],
  [1, 0, 1],
  [0, 1, 0]];
  let graph2 = [[0, 1, 1, 1, 0],
  [1, 0, 1, 0, 0],
  [1, 1, 0, 0, 0],
  [1, 0, 0, 0, 1],
  [0, 0, 0, 1, 0]];
  var connected = isBiconnected(graph1);
  console.log("Graph 1 is Biconnected= " + connected);
  connected = isBiconnected(graph2);
  console.log("Graph 2 is Biconnected= " + connected);
}

/** IsBicconected()  function takes graph as input return boolean value 
 * here we are checking biconnectivity by performing dfs at each vertex on  graph .
 * here the concept is remove one vertex and perform dfs to check we can visit all node.
 * if we are not able to visit any vertex than that graph is not biconnected.
*/
function isBiconnected(graph) {
  var tempGraph = graph;
  for (let i = 0; i < graph.length; i++) {
    graph = tempGraph;
    let visited = [];
    for (let k = 0; k < graph.length; k++)
      visited[k] = 0;
    for (let j = 0; j < graph.length; j++) {
      graph[i][j] = 0;
      graph[j][i] = 0;
    }

    //console.log(visited);
    visited[(i + 1) % (graph.length - 1)] = 1;
    //console.log((i + 1) % (graph.length - 1));
    dfs(graph, visited, (i + 1) % (graph.length - 1));
    // console.log(visited);
    for (let k = 0; k < graph.length; k++) {
      if (visited[k] == 0 && k != i)
        return false;
    }
    return true;
  }
}

/**dfs() is the function for depth first travesal 
 * the concept is make an visited array of size total no vertex and initalize it with 0;
 * whenever we visit a vertex for first time make its value as 1 so at another iteration 
 * we dont have to traverse this vertex;
 */
function dfs(graph, visited, i) {
  for (let j = 0; j < graph.length; j++) {
    if (graph[i][j] == 1 && visited[j] != 1) {
      visited[j] = 1;
      dfs(graph, visited, j);
    }

  }
  return;
}
/**run() function initialize the graph and graph is implemented using matrix */
function run() {
  let graph = [[0, 1, 1, 0],
  [0, 0, 0, 0],
  [0, 0, 0, 1],
  [1, 0, 0, 0]];

  var connected = isCyclic(graph);
  console.log(connected);
}

/**isCycle() function takes graph as input and return boolean value either true or false
 * & the concept is start from any vertex and perform dfs inaddition with to check for backedge;
 * if backedge is present than ther eis cycle otherwise no cycle;
 * to check for backedge when we are visitng any node we put it in backedge list if any non visted node 
 * has an edge with the node in backedge list than that edge is backedge and we stop.
 */
function isCyclic(graph) {
  let visited = [];
  for (let i = 0; i < graph.length; i++)
    visited[i] = 0;
  visited[0] = 1;
  let backedge = [0];
  return dfs(graph, visited, 0, backedge);
}

/**dfs() is the function for depth first travesal
 * the concept is make an visited array of size total no vertex and initalize it with 0;
 * whenever we visit a vertex for first time make its value as 1 so at another iteration
 * we dont have to traverse this vertex;
 */
function dfs(graph, visited, u, backedge) {
  // console.log("backedge");
  //console.log(backedge);
  var flag = false;
  for (let v = 0; v < graph.length; v++) {
    if (v in backedge && graph[u][v] == 1)
      return true;
    if (graph[u][v] == 1 && visited[v] != 1) {
      //console.log(v in backedge);
      visited[v] = 1;
      backedge.push(v);
      flag = dfs(graph, visited, v, backedge)
      if (flag)
        return flag;
    }
  }
  return flag;
}
